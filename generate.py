import json
def get_igs_stations():
  load = json.loads(open('igs.json').read())
  data = {}
  for x in load:
    data[x['siteID'][:4]] = {
      'latitude': x['lat'],
      'longitude': x['lon']
    }
  return data

def print_to_file(data):
  encoded = []
  for x in data:
    encoded.append(
        {
          "key": x[:4],
          "latitude": data[x]['latitude'],
          "longitude": data[x]['longitude'],
          "name": x[:4],
        }
        )
  with open('result.json','w') as f:
    f.write(
            json.dumps(encoded,
            sort_keys=True,
            indent=4,
            separators=(',', ': ')))

def main():
  igs = get_igs_stations()
  print_to_file(igs)


if __name__ == "__main__":
  main()