clean:
	rm -f igs.json
	rm -f result.json

igs.json:
	curl 'http://www.igs.org/php/getSLMSitesCoords.php?networkMode=init' \
	  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
	  -H 'Referer: http://www.igs.org/network' \
	  -H 'DNT: 1' \
	  -H 'X-Requested-With: XMLHttpRequest' \
	  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36' \
	  --compressed \
	  --insecure > igs.json

join: igs.json
	python3 generate.py


build: clean join

